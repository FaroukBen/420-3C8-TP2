import java.util.*;
public class LiasseVierge extends Liasse{
ArrayList<Document> documents;
private static LiasseVierge _instance = null;
private LiasseVierge(){
documents = new ArrayList<Document>();
}
ArrayList<Document> getDocuments(){
return documents;
}
public static LiasseVierge Instance() {
if (_instance == null)
_instance = new LiasseVierge();
return _instance; }}
